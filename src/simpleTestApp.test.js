const startServer = require("./simpleTestApp");
const supertest = require("supertest");

let server
let app;
const mockMorgan = jest.fn((req,res,next)=>next())

beforeAll(async()=>{
    jest.mock('morgan', () => () => mockMorgan);
    server = await startServer()
    app = supertest(server)
});
afterAll(()=>{
    jest.unmock("morgan");
    server.stop();
});


describe('GET',()=>{
    it("should return an array of tasks",async()=>{
        const response = await app.get("/")
        .expect('Content-Type', /json/)
        .expect(200)
    })
})
describe('POST',()=>{
    it("should accept a valid task",async()=>{
        const response = await app.post('/')
        .type("json")
        .send({
            title: 'do the task',
            description: 'testtesttest task lorem ipsum'
        })
        expect(response.status).toBe(200)
    })
    it("should reject a task if it fails the validation(title too short)",async()=>{
        const response = await app.post('/')
        .type("json")
        .send({
            title: 'do',
            description: 'testtesttest task lorem ipsum'
        })
        expect(response.status).toBe(400)
    })
    it("should reject a task if it fails the validation(no description)",async()=>{
        const response = await app.post('/')
        .type("json")
        .send({
            title: 'do the task'
        })
        expect(response.status).toBe(400)
    })
    it("should reject a task if it fails the validation(number for title)",async()=>{
        const response = await app.post('/')
        .type("json")
        .send({
            title: 123456,
            description: 'testtesttest task lorem ipsum'
        })
        expect(response.status).toBe(400)
    })
})
const getFibonacci = require("./fibonacci");

describe("fibonacci", function () {
    it('should return a correct fibonacci number',async()=>{
        expect(getFibonacci(2)).toEqual(1)
        expect(getFibonacci(5)).toEqual(5)
        expect(getFibonacci(9)).toEqual(34)
        expect(getFibonacci(11)).toEqual(89)
    })
    it('should return 0 for input of 0',async()=>{
        expect(getFibonacci(0)).toEqual(0)
    })
    it('should return 1 for input of 1',async()=>{
        expect(getFibonacci(1)).toEqual(1)
    })
    it('should return 0 for any negative input',async()=>{
        expect(getFibonacci(-2)).toEqual(0)
        expect(getFibonacci(-5)).toEqual(0)
        expect(getFibonacci(-9)).toEqual(0)
        expect(getFibonacci(-11)).toEqual(0)
        expect(getFibonacci(-123456)).toEqual(0)
    })
});

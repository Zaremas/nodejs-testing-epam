const findText = require("./findText");
jest.mock("./getFileContent", () => {
  return jest.fn();
});

const getFileContent = require("./getFileContent");

describe("findText", function () {
  
  afterEach(() => {
    getFileContent.mockReset();
  });

  it("should return true when file content contains textToFind", async () => {
    const fileContent = "Lorem ipsum.";

    getFileContent.mockResolvedValue(fileContent);

    const result = await findText("abc", "or");
    expect(result).toBe(true);
  });

  it("should return false when file content doesn't contain textToFind", async () => {
    const fileContent = "Lorem ipsum.";

    getFileContent.mockResolvedValue(fileContent);

    const result = await findText("abc", "and");
    expect(result).toBe(false);
  });

  it("should reject promise when getFileContent rejects promise", async () => {
    const errorMessage = "File not found";

    getFileContent.mockRejectedValue(new Error(errorMessage));

    await expect(findText("abc","aaa")).rejects.toThrow(errorMessage);
  });
});
